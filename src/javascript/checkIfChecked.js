
export function checkCheckbox(elem) {
    let amount = 6;
    if (elem.checked) {
        checkCheckbox.check += 1; 
    } else {
        checkCheckbox.check -= 1; 
    }

    if (checkCheckbox.check == '2') {
        for (let i = 1; i <= amount; i++) {
            let my = 'check'+ i;
            if (document.getElementById(my).checked == false) {
                document.getElementById(my).disabled=true;
            }
        }
    } else {
        for (let i = 1; i <= amount; i++) {
            let my = 'check'+ i;
            document.getElementById(my).disabled=false;
        } 
        
    } 

}

checkCheckbox.check = 0;