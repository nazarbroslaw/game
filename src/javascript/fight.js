import {Fighter} from './fighter';
import FightersView from './fightersView';

export  async function fight(fighterFirst, fighterSecond) {

    let fighterOne = new Fighter(fighterFirst);
    let fighterTwo = new Fighter(fighterSecond);
    
    let hitPowerFighterOne = await fighterOne.getHitPower();
    let blockPowerFighterOne = await fighterOne.getBlockPower();

    let hitPowerFighterTwo = await fighterTwo.getHitPower();
    let blockPowerFighterTwo = await fighterTwo.getBlockPower();

    document.querySelector(".container").classList.add("fight");
    let twoFighters = [];
    let allFighters = [...document.querySelectorAll('input[class^="check"]')];
    await allFighters.forEach( (item, i) => {
        if(item.checked !== true) {

            item.parentNode.classList.add("fight");
        } else if(item.checked == true) {
            twoFighters.push(item);
            item.classList.add("fight");
            
        }
    });
       let healthOne = hitPowerFighterOne[1];
       let healthTwo = hitPowerFighterTwo[1];
       

        let timerOne = setInterval(()=> {
            twoFighters[0].parentNode.classList.toggle("positionFirstFighter");
            healthOne = healthOne - (hitPowerFighterTwo[0] - blockPowerFighterOne[0]);
            if(healthOne <= 0) {
                clearInterval(timerOne);
                clearInterval(timerTwo);
                twoFighters[1].parentNode.classList.add("loser");
                let elem = document.createElement("div");
                elem.innerHTML = `<p class='winner'>WINNER</p>
                <p class="warn">in order to play again reload the page</p>`
                twoFighters[0].parentNode.appendChild(elem);

            }
        }, 100);

        let timerTwo = setInterval(()=> {
            twoFighters[1].parentNode.classList.toggle("positionSecondFighter");
            healthTwo = healthTwo - (hitPowerFighterOne[0] - blockPowerFighterTwo[0]);
            if(healthTwo <= 0) {
                clearInterval(timerOne);
                clearInterval(timerTwo);
                twoFighters[0].parentNode.classList.add("loser");
                let elem = document.createElement("div");
                elem.innerHTML = `<p class='winner'>WINNER</p>
                <p class="warn">in order to play again reload the page</p>`
                twoFighters[1].parentNode.appendChild(elem);
            }
        }, 100);
        

}


