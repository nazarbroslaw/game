import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';

class FightersView extends View {
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
    this.fightersDetailsMap = new Map();

  }
  static hello = this.fightersDetailsMap;
  

  createFighters(fighters) {
    console.log(fighters);
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

    async handleFighterClick(event, fighter) {
    // this.fightersDetailsMap.set(fighter._id, fighter);
    // console.log(this.fightersDetailsMap);
    if(this.fightersDetailsMap.get(fighter._id) == undefined) {
      await fighterService.getFighterDetails(fighter._id)
      .then(value => {
        this.fightersDetailsMap.set(fighter._id, value);

      });
    }

    console.log(this.fightersDetailsMap);
    let feature = this.fightersDetailsMap.get(fighter._id);
    this.modalWindow = this.createElement({tagName: 'div', className: `fighter${fighter['_id']}`});
    this.modalWindow.classList.add("modalwindow");

    this.modalWindow.innerHTML =
    `<form action="">
    <form action="">
    <fieldset>
      <p class="message">to close click on fighter again</p>
      <legend class="addinfo">Addition info of fighter</legend>
      <label for="health">Health:</label>
      <input id="health" type="number" required min="0" placeholder="input a number" value="${feature.health}"/><br>
      <label for="attack">Attack:</label>
      <input id="attack" type="number" required min="0" placeholder="input a number" value="${feature.attack}"/><br>	
      <label for="defense">Defense:</label>
      <input id="defense" type="number" required min="0" placeholder="input a number" value="${feature.defense}"/><br>	
      <button class="change-parameters" id="change-param">Change</button>

    </fieldset>
   </form>`;
    
    try {
      if(event.target.className.includes("fighter") 
      && event.target.tagName == 'DIV') {
        if(!event.target.lastChild.className.includes('modalwindow')) 
          event.target.append(this.modalWindow);
        else event.target.removeChild(event.target.lastChild);
        
      } else {
        if(!event.target.parentNode.lastChild.className.includes('modalwindow'))
          event.target.parentNode.append(this.modalWindow);
        else event.target.parentNode.removeChild(event.target.parentNode.lastChild); 
      }
    } catch{}
    

    if(document.querySelector(".change-parameters")) {
      document.querySelector(".change-parameters").addEventListener('click', (event) => {
        event.preventDefault();

        feature.health = document.getElementById("health").value;
        feature.attack = document.getElementById("attack").value;
        feature.defense = document.getElementById("defense").value;

      });
    }

  }
}

export default FightersView;
