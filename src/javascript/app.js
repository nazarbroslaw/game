import FightersView from './fightersView';
import { fighterService } from './services/fightersService';
import { checkCheckbox} from './checkIfChecked';
import {fight} from './fight';
class App {
  constructor() {
    this.startApp();
  }
  
  static rootElement = document.getElementById('root');
  static loadingElement = document.getElementById('loading-overlay');
  
  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';
      
      const fighters = await fighterService.getFighters();
      const fightersView = new FightersView(fighters);
      this.handle = fightersView.handleClick;
      fight.map = fightersView;
      const fightersElement = fightersView.element;
      async function addElement() {

        await App.rootElement.appendChild(fightersElement);
        let i = 0;
        let arrFighter = [...document.querySelectorAll(".fighter")];
        arrFighter.forEach((item) => {
          let newEl = document.createElement("input");
          newEl.setAttribute("type", "checkbox");
          newEl.classList.add("check-fighter");
          newEl.id = `check${++i}`;
          // newEl.setAttribute("onclick", "event.stopImmediatePropagation()");
          newEl.addEventListener("click", function(event) {
            event.stopImmediatePropagation();
            checkCheckbox(newEl);
          });
          item.insertBefore(newEl, item.querySelector(".fighter-image"));  
        })

        document.querySelector(".start-game").addEventListener('click', (event) => {
          let checkboxFighters = [...document.querySelectorAll(".check-fighter")];
          let flag = 0;
          let arrFighter = [];
          checkboxFighters.forEach((item) => {
            if(item.checked == true) {
              arrFighter.push(item);
              flag++;
            }
          });

          if(flag == 2) {
            event.target.disabled = "true";
            fight(arrFighter[0].id.slice(5), arrFighter[1].id.slice(5));
          } else {
            document.querySelector(".instruction").classList.add("warning");
            setTimeout(() => {
                  document.querySelector(".instruction").classList.toggle("warning");
                }, 1000);
          }
        });
        
      }

      addElement();

    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }
}

export default App;