import fightersView from './fightersView';
import {fighterService} from './services/fightersService';
import {fight} from './fight';

export class Fighter {
    constructor(idFighter) {
        this.idFighter = idFighter;
        
        this.fighters = fight.map.fightersDetailsMap;

        // this.addInformation = this.getAdditionalInformation();
        // this.power = this.getPower();
        // this.fr = this.getBlockPower();
    }
    

     async getAdditionalInformation() {
        // console.log(this.idFighter);
        if(this.fighters.get(this.idFighter) == undefined) {
          await fighterService.getFighterDetails(this.idFighter)
          .then((value) => {
               this.fighters.set(this.idFighter, value);
               
            });
        }
        return this.fighters;
    }

    async getHitPower() {
        let power;
        let health;
       await this.getAdditionalInformation().then(value => {
        let criticalHitChance = (1 + Math.floor(Math.random() * 2));
        power =  value.get(this.idFighter).attack * criticalHitChance;
        health = value.get(this.idFighter).health;
       });

        return [power, health];
   }

    async getBlockPower() {
        let power;
        let health;
        await this.getAdditionalInformation().then(value => {
         let dodgeChance = (1 + Math.floor(Math.random() * 2));
         power =  value.get(this.idFighter).defense *  dodgeChance ;
         health = value.get(this.idFighter).health;
        });
 
         return [power, health];

   }

}